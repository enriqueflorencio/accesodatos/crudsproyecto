<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alias".
 *
 * @property string $cod_personaje
 * @property string $alias
 *
 * @property Personajes $codPersonaje
 */
class Alias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_personaje', 'alias'], 'required'],
            [['cod_personaje'], 'string', 'max' => 3],
            [['alias'], 'string', 'max' => 255],
            [['cod_personaje', 'alias'], 'unique', 'targetAttribute' => ['cod_personaje', 'alias']],
            [['cod_personaje'], 'exist', 'skipOnError' => true, 'targetClass' => Personajes::className(), 'targetAttribute' => ['cod_personaje' => 'cod_personaje']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_personaje' => 'Cod Personaje',
            'alias' => 'Alias',
        ];
    }

    /**
     * Gets query for [[CodPersonaje]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPersonaje()
    {
        return $this->hasOne(Personajes::className(), ['cod_personaje' => 'cod_personaje']);
    }
}
