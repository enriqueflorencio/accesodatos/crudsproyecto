<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estan".
 *
 * @property int $id
 * @property string|null $cod_personaje
 * @property string|null $cod_libro
 *
 * @property Libros $codLibro
 * @property Personajes $codPersonaje
 */
class Estan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_personaje'], 'string', 'max' => 3],
            [['cod_libro'], 'string', 'max' => 2],
            [['cod_personaje', 'cod_libro'], 'unique', 'targetAttribute' => ['cod_personaje', 'cod_libro']],
            [['cod_libro'], 'exist', 'skipOnError' => true, 'targetClass' => Libros::className(), 'targetAttribute' => ['cod_libro' => 'cod_libro']],
            [['cod_personaje'], 'exist', 'skipOnError' => true, 'targetClass' => Personajes::className(), 'targetAttribute' => ['cod_personaje' => 'cod_personaje']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cod_personaje' => 'Cod Personaje',
            'cod_libro' => 'Cod Libro',
        ];
    }

    /**
     * Gets query for [[CodLibro]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodLibro()
    {
        return $this->hasOne(Libros::className(), ['cod_libro' => 'cod_libro']);
    }

    /**
     * Gets query for [[CodPersonaje]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPersonaje()
    {
        return $this->hasOne(Personajes::className(), ['cod_personaje' => 'cod_personaje']);
    }
}
