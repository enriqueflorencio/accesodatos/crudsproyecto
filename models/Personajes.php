<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "personajes".
 *
 * @property string $cod_personaje
 * @property string|null $nombre
 * @property int|null $vivo
 * @property int|null $saltamundo
 *
 * @property Alias[] $aliases
 * @property Estan[] $estans
 * @property Libros[] $codLibros
 * @property Pertenecen[] $pertenecens
 * @property Organizaciones[] $codOrganizacions
 * @property Poseen[] $poseens
 * @property Fragmentos[] $codFragmentos
 * @property Tienen[] $tienens
 * @property Habilidades[] $codHabilidads
 * @property Viven[] $vivens
 * @property Mundos[] $codMundos
 */
class Personajes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personajes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_personaje'], 'required'],
            [['vivo', 'saltamundo'], 'integer'],
            [['cod_personaje'], 'string', 'max' => 3],
            [['nombre'], 'string', 'max' => 100],
            [['cod_personaje'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_personaje' => 'Cod Personaje',
            'nombre' => 'Nombre',
            'vivo' => 'Vivo',
            'saltamundo' => 'Saltamundo',
        ];
    }

    /**
     * Gets query for [[Aliases]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAliases()
    {
        return $this->hasMany(Alias::className(), ['cod_personaje' => 'cod_personaje']);
    }

    /**
     * Gets query for [[Estans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstans()
    {
        return $this->hasMany(Estan::className(), ['cod_personaje' => 'cod_personaje']);
    }

    /**
     * Gets query for [[CodLibros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodLibros()
    {
        return $this->hasMany(Libros::className(), ['cod_libro' => 'cod_libro'])->viaTable('estan', ['cod_personaje' => 'cod_personaje']);
    }

    /**
     * Gets query for [[Pertenecens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPertenecens()
    {
        return $this->hasMany(Pertenecen::className(), ['cod_personaje' => 'cod_personaje']);
    }

    /**
     * Gets query for [[CodOrganizacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodOrganizacions()
    {
        return $this->hasMany(Organizaciones::className(), ['cod_organizacion' => 'cod_organizacion'])->viaTable('pertenecen', ['cod_personaje' => 'cod_personaje']);
    }

    /**
     * Gets query for [[Poseens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPoseens()
    {
        return $this->hasMany(Poseen::className(), ['cod_personaje' => 'cod_personaje']);
    }

    /**
     * Gets query for [[CodFragmentos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodFragmentos()
    {
        return $this->hasMany(Fragmentos::className(), ['cod_fragmento' => 'cod_fragmento'])->viaTable('poseen', ['cod_personaje' => 'cod_personaje']);
    }

    /**
     * Gets query for [[Tienens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTienens()
    {
        return $this->hasMany(Tienen::className(), ['cod_personaje' => 'cod_personaje']);
    }

    /**
     * Gets query for [[CodHabilidads]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodHabilidads()
    {
        return $this->hasMany(Habilidades::className(), ['cod_habilidad' => 'cod_habilidad'])->viaTable('tienen', ['cod_personaje' => 'cod_personaje']);
    }

    /**
     * Gets query for [[Vivens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVivens()
    {
        return $this->hasMany(Viven::className(), ['cod_personaje' => 'cod_personaje']);
    }

    /**
     * Gets query for [[CodMundos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodMundos()
    {
        return $this->hasMany(Mundos::className(), ['cod_mundo' => 'cod_mundo'])->viaTable('viven', ['cod_personaje' => 'cod_personaje']);
    }
}
