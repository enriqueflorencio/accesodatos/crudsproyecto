<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "libros".
 *
 * @property string $cod_libro
 * @property string|null $titulo
 * @property string|null $sinopsis
 * @property int|null $num_capitulos
 * @property int|null $num_paginas
 * @property string|null $f_publicacion
 * @property string|null $cod_saga
 *
 * @property Estan[] $estans
 * @property Personajes[] $codPersonajes
 * @property Sagas $codSaga
 */
class Libros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'libros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_libro'], 'required'],
            [['sinopsis'], 'string'],
            [['num_capitulos', 'num_paginas'], 'integer'],
            [['f_publicacion'], 'safe'],
            [['cod_libro'], 'string', 'max' => 2],
            [['titulo'], 'string', 'max' => 100],
            [['cod_saga'], 'string', 'max' => 3],
            [['cod_libro'], 'unique'],
            [['cod_saga'], 'exist', 'skipOnError' => true, 'targetClass' => Sagas::className(), 'targetAttribute' => ['cod_saga' => 'cod_saga']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_libro' => 'Cod Libro',
            'titulo' => 'Titulo',
            'sinopsis' => 'Sinopsis',
            'num_capitulos' => 'Num Capitulos',
            'num_paginas' => 'Num Paginas',
            'f_publicacion' => 'F Publicacion',
            'cod_saga' => 'Cod Saga',
        ];
    }

    /**
     * Gets query for [[Estans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstans()
    {
        return $this->hasMany(Estan::className(), ['cod_libro' => 'cod_libro']);
    }

    /**
     * Gets query for [[CodPersonajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPersonajes()
    {
        return $this->hasMany(Personajes::className(), ['cod_personaje' => 'cod_personaje'])->viaTable('estan', ['cod_libro' => 'cod_libro']);
    }

    /**
     * Gets query for [[CodSaga]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodSaga()
    {
        return $this->hasOne(Sagas::className(), ['cod_saga' => 'cod_saga']);
    }
}
