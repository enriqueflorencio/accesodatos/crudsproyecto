<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mundos".
 *
 * @property string $cod_mundo
 * @property string|null $nombre
 *
 * @property Viven[] $vivens
 * @property Personajes[] $codPersonajes
 */
class Mundos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mundos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_mundo'], 'required'],
            [['cod_mundo'], 'string', 'max' => 3],
            [['nombre'], 'string', 'max' => 25],
            [['cod_mundo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_mundo' => 'Cod Mundo',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Vivens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVivens()
    {
        return $this->hasMany(Viven::className(), ['cod_mundo' => 'cod_mundo']);
    }

    /**
     * Gets query for [[CodPersonajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPersonajes()
    {
        return $this->hasMany(Personajes::className(), ['cod_personaje' => 'cod_personaje'])->viaTable('viven', ['cod_mundo' => 'cod_mundo']);
    }
}
