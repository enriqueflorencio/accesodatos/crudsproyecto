<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pertenecen".
 *
 * @property int $id
 * @property string|null $cod_organizacion
 * @property string|null $cod_personaje
 *
 * @property Organizaciones $codOrganizacion
 * @property Personajes $codPersonaje
 */
class Pertenecen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pertenecen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_organizacion'], 'string', 'max' => 2],
            [['cod_personaje'], 'string', 'max' => 3],
            [['cod_personaje', 'cod_organizacion'], 'unique', 'targetAttribute' => ['cod_personaje', 'cod_organizacion']],
            [['cod_organizacion'], 'exist', 'skipOnError' => true, 'targetClass' => Organizaciones::className(), 'targetAttribute' => ['cod_organizacion' => 'cod_organizacion']],
            [['cod_personaje'], 'exist', 'skipOnError' => true, 'targetClass' => Personajes::className(), 'targetAttribute' => ['cod_personaje' => 'cod_personaje']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cod_organizacion' => 'Cod Organizacion',
            'cod_personaje' => 'Cod Personaje',
        ];
    }

    /**
     * Gets query for [[CodOrganizacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodOrganizacion()
    {
        return $this->hasOne(Organizaciones::className(), ['cod_organizacion' => 'cod_organizacion']);
    }

    /**
     * Gets query for [[CodPersonaje]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPersonaje()
    {
        return $this->hasOne(Personajes::className(), ['cod_personaje' => 'cod_personaje']);
    }
}
