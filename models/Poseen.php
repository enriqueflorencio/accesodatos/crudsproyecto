<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "poseen".
 *
 * @property int $id
 * @property string|null $cod_personaje
 * @property string|null $cod_fragmento
 *
 * @property Fragmentos $codFragmento
 * @property Personajes $codPersonaje
 */
class Poseen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'poseen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_personaje', 'cod_fragmento'], 'string', 'max' => 3],
            [['cod_personaje', 'cod_fragmento'], 'unique', 'targetAttribute' => ['cod_personaje', 'cod_fragmento']],
            [['cod_fragmento'], 'exist', 'skipOnError' => true, 'targetClass' => Fragmentos::className(), 'targetAttribute' => ['cod_fragmento' => 'cod_fragmento']],
            [['cod_personaje'], 'exist', 'skipOnError' => true, 'targetClass' => Personajes::className(), 'targetAttribute' => ['cod_personaje' => 'cod_personaje']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cod_personaje' => 'Cod Personaje',
            'cod_fragmento' => 'Cod Fragmento',
        ];
    }

    /**
     * Gets query for [[CodFragmento]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodFragmento()
    {
        return $this->hasOne(Fragmentos::className(), ['cod_fragmento' => 'cod_fragmento']);
    }

    /**
     * Gets query for [[CodPersonaje]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPersonaje()
    {
        return $this->hasOne(Personajes::className(), ['cod_personaje' => 'cod_personaje']);
    }
}
