<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "habilidades".
 *
 * @property string $cod_habilidad
 * @property string|null $nombre
 * @property string|null $descripcion
 *
 * @property Tienen[] $tienens
 * @property Personajes[] $codPersonajes
 */
class Habilidades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'habilidades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_habilidad'], 'required'],
            [['descripcion'], 'string'],
            [['cod_habilidad'], 'string', 'max' => 3],
            [['nombre'], 'string', 'max' => 100],
            [['cod_habilidad'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_habilidad' => 'Cod Habilidad',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[Tienens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTienens()
    {
        return $this->hasMany(Tienen::className(), ['cod_habilidad' => 'cod_habilidad']);
    }

    /**
     * Gets query for [[CodPersonajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPersonajes()
    {
        return $this->hasMany(Personajes::className(), ['cod_personaje' => 'cod_personaje'])->viaTable('tienen', ['cod_habilidad' => 'cod_habilidad']);
    }
}
