<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "viven".
 *
 * @property int $id
 * @property string|null $cod_personaje
 * @property string|null $cod_mundo
 *
 * @property Mundos $codMundo
 * @property Personajes $codPersonaje
 */
class Viven extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'viven';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_personaje', 'cod_mundo'], 'string', 'max' => 3],
            [['cod_personaje', 'cod_mundo'], 'unique', 'targetAttribute' => ['cod_personaje', 'cod_mundo']],
            [['cod_mundo'], 'exist', 'skipOnError' => true, 'targetClass' => Mundos::className(), 'targetAttribute' => ['cod_mundo' => 'cod_mundo']],
            [['cod_personaje'], 'exist', 'skipOnError' => true, 'targetClass' => Personajes::className(), 'targetAttribute' => ['cod_personaje' => 'cod_personaje']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cod_personaje' => 'Cod Personaje',
            'cod_mundo' => 'Cod Mundo',
        ];
    }

    /**
     * Gets query for [[CodMundo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodMundo()
    {
        return $this->hasOne(Mundos::className(), ['cod_mundo' => 'cod_mundo']);
    }

    /**
     * Gets query for [[CodPersonaje]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPersonaje()
    {
        return $this->hasOne(Personajes::className(), ['cod_personaje' => 'cod_personaje']);
    }
}
