<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fragmentos".
 *
 * @property string $cod_fragmento
 * @property string|null $nombre
 * @property int|null $destruido
 *
 * @property Poseen[] $poseens
 * @property Personajes[] $codPersonajes
 */
class Fragmentos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fragmentos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_fragmento'], 'required'],
            [['destruido'], 'integer'],
            [['cod_fragmento'], 'string', 'max' => 3],
            [['nombre'], 'string', 'max' => 25],
            [['cod_fragmento'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_fragmento' => 'Cod Fragmento',
            'nombre' => 'Nombre',
            'destruido' => 'Destruido',
        ];
    }

    /**
     * Gets query for [[Poseens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPoseens()
    {
        return $this->hasMany(Poseen::className(), ['cod_fragmento' => 'cod_fragmento']);
    }

    /**
     * Gets query for [[CodPersonajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPersonajes()
    {
        return $this->hasMany(Personajes::className(), ['cod_personaje' => 'cod_personaje'])->viaTable('poseen', ['cod_fragmento' => 'cod_fragmento']);
    }
}
