<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sagas".
 *
 * @property string $cod_saga
 * @property string|null $titulo
 * @property int|null $num_libros
 * @property int|null $paginas_totales
 *
 * @property Libros[] $libros
 */
class Sagas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sagas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_saga'], 'required'],
            [['num_libros', 'paginas_totales'], 'integer'],
            [['cod_saga'], 'string', 'max' => 3],
            [['titulo'], 'string', 'max' => 100],
            [['cod_saga'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_saga' => 'Cod Saga',
            'titulo' => 'Titulo',
            'num_libros' => 'Num Libros',
            'paginas_totales' => 'Paginas Totales',
        ];
    }

    /**
     * Gets query for [[Libros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLibros()
    {
        return $this->hasMany(Libros::className(), ['cod_saga' => 'cod_saga']);
    }
}
