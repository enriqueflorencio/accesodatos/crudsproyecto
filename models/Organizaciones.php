<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organizaciones".
 *
 * @property string $cod_organizacion
 * @property string|null $nombre
 * @property string|null $nombre_fundador
 * @property int|null $num_integrantes
 *
 * @property Pertenecen[] $pertenecens
 * @property Personajes[] $codPersonajes
 */
class Organizaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'organizaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_organizacion'], 'required'],
            [['num_integrantes'], 'integer'],
            [['cod_organizacion'], 'string', 'max' => 2],
            [['nombre'], 'string', 'max' => 25],
            [['nombre_fundador'], 'string', 'max' => 100],
            [['cod_organizacion'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_organizacion' => 'Cod Organizacion',
            'nombre' => 'Nombre',
            'nombre_fundador' => 'Nombre Fundador',
            'num_integrantes' => 'Num Integrantes',
        ];
    }

    /**
     * Gets query for [[Pertenecens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPertenecens()
    {
        return $this->hasMany(Pertenecen::className(), ['cod_organizacion' => 'cod_organizacion']);
    }

    /**
     * Gets query for [[CodPersonajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPersonajes()
    {
        return $this->hasMany(Personajes::className(), ['cod_personaje' => 'cod_personaje'])->viaTable('pertenecen', ['cod_organizacion' => 'cod_organizacion']);
    }
}
