<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tienen".
 *
 * @property int $id
 * @property string|null $cod_personaje
 * @property string|null $cod_habilidad
 *
 * @property Habilidades $codHabilidad
 * @property Personajes $codPersonaje
 */
class Tienen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tienen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_personaje', 'cod_habilidad'], 'string', 'max' => 3],
            [['cod_personaje', 'cod_habilidad'], 'unique', 'targetAttribute' => ['cod_personaje', 'cod_habilidad']],
            [['cod_habilidad'], 'exist', 'skipOnError' => true, 'targetClass' => Habilidades::className(), 'targetAttribute' => ['cod_habilidad' => 'cod_habilidad']],
            [['cod_personaje'], 'exist', 'skipOnError' => true, 'targetClass' => Personajes::className(), 'targetAttribute' => ['cod_personaje' => 'cod_personaje']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cod_personaje' => 'Cod Personaje',
            'cod_habilidad' => 'Cod Habilidad',
        ];
    }

    /**
     * Gets query for [[CodHabilidad]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodHabilidad()
    {
        return $this->hasOne(Habilidades::className(), ['cod_habilidad' => 'cod_habilidad']);
    }

    /**
     * Gets query for [[CodPersonaje]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPersonaje()
    {
        return $this->hasOne(Personajes::className(), ['cod_personaje' => 'cod_personaje']);
    }
}
