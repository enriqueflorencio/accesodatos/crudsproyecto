<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Poseen */

$this->title = 'Create Poseen';
$this->params['breadcrumbs'][] = ['label' => 'Poseens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poseen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
