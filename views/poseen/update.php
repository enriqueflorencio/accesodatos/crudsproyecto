<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Poseen */

$this->title = 'Update Poseen: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Poseens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="poseen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
