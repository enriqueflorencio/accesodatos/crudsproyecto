<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Sagas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sagas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_saga')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'titulo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'num_libros')->textInput() ?>

    <?= $form->field($model, 'paginas_totales')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
