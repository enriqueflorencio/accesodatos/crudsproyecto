<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sagas */

$this->title = 'Create Sagas';
$this->params['breadcrumbs'][] = ['label' => 'Sagas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sagas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
