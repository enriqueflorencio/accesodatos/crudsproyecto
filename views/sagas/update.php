<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sagas */

$this->title = 'Update Sagas: ' . $model->cod_saga;
$this->params['breadcrumbs'][] = ['label' => 'Sagas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_saga, 'url' => ['view', 'id' => $model->cod_saga]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sagas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
