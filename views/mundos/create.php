<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mundos */

$this->title = 'Create Mundos';
$this->params['breadcrumbs'][] = ['label' => 'Mundos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mundos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
