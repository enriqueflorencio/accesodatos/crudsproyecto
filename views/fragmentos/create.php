<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Fragmentos */

$this->title = 'Create Fragmentos';
$this->params['breadcrumbs'][] = ['label' => 'Fragmentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fragmentos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
