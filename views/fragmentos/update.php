<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Fragmentos */

$this->title = 'Update Fragmentos: ' . $model->cod_fragmento;
$this->params['breadcrumbs'][] = ['label' => 'Fragmentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_fragmento, 'url' => ['view', 'id' => $model->cod_fragmento]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fragmentos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
