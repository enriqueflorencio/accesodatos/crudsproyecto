<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tienen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tienen-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_personaje')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cod_habilidad')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
