<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Organizaciones */

$this->title = 'Update Organizaciones: ' . $model->cod_organizacion;
$this->params['breadcrumbs'][] = ['label' => 'Organizaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_organizacion, 'url' => ['view', 'id' => $model->cod_organizacion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="organizaciones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
