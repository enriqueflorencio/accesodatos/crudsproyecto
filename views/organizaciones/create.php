<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Organizaciones */

$this->title = 'Create Organizaciones';
$this->params['breadcrumbs'][] = ['label' => 'Organizaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organizaciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
