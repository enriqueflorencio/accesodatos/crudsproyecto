<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Organizaciones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="organizaciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_organizacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_fundador')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'num_integrantes')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
