<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Estan */

$this->title = 'Create Estan';
$this->params['breadcrumbs'][] = ['label' => 'Estans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
