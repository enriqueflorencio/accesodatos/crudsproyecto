<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Estan */

$this->title = 'Update Estan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Estans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="estan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
