<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Alias */

$this->title = 'Update Alias: ' . $model->cod_personaje;
$this->params['breadcrumbs'][] = ['label' => 'Aliases', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_personaje, 'url' => ['view', 'cod_personaje' => $model->cod_personaje, 'alias' => $model->alias]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="alias-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
