<?php

namespace app\controllers;

use Yii;
use app\models\Alias;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AliasController implements the CRUD actions for Alias model.
 */
class AliasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Alias models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Alias::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Alias model.
     * @param string $cod_personaje
     * @param string $alias
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cod_personaje, $alias)
    {
        return $this->render('view', [
            'model' => $this->findModel($cod_personaje, $alias),
        ]);
    }

    /**
     * Creates a new Alias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Alias();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_personaje' => $model->cod_personaje, 'alias' => $model->alias]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Alias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $cod_personaje
     * @param string $alias
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cod_personaje, $alias)
    {
        $model = $this->findModel($cod_personaje, $alias);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_personaje' => $model->cod_personaje, 'alias' => $model->alias]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Alias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $cod_personaje
     * @param string $alias
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cod_personaje, $alias)
    {
        $this->findModel($cod_personaje, $alias)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Alias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $cod_personaje
     * @param string $alias
     * @return Alias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cod_personaje, $alias)
    {
        if (($model = Alias::findOne(['cod_personaje' => $cod_personaje, 'alias' => $alias])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
